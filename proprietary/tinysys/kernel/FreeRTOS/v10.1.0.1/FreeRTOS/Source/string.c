/*-
 * Copyright (c) 1990, 1993
 *	The Regents of the University of California.  All rights reserved.
 *
 * This code is derived from software contributed to Berkeley by
 * Mike Hibler and Chris Torek.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <string.h>
#include <stdint.h>
#include <ctype.h>

/*
 * sizeof(word) MUST BE A POWER OF TWO
 * SO THAT wmask BELOW IS ALL ONES
 */
typedef	int word;		/* "word" used for optimal copy speed */

#define	wsize	sizeof(word)
#define	wmask	(wsize - 1)
#define	dqwsize	sizeof(word) * 8
#define	dqwmask	(dqwsize - 1)

/*
 * Macros: loop-t-times; and loop-t-times, t>0
 */
#define	TLOOP(s) if (t) TLOOP1(s)
#define	TLOOP1(s) do { s; } while (--t)

#if defined (MRV55)
static void *dqword_copy(void *dst, const void *src, size_t length);
#endif

/*
 * Copy a block of memory, handling overlap.
 * This is the routine that actually implements
 * (the portable versions of) bcopy, memcpy, and memmove.
 */
#pragma GCC diagnostic push  // require GCC 4.6
#pragma GCC diagnostic ignored "-Wcast-align"
void *memcpy(void *dst0, const void *src0, size_t length)
{
	char *dst = dst0;
	const char *src = src0;
	size_t t;

	if (length == 0 || dst == src)		/* nothing to do */
		goto done;

	if ((unsigned long)dst < (unsigned long)src) {
		/*
		 * Copy forward.
		 */
		t = (uintptr_t)src;	/* only need low bits */
		if ((t | (uintptr_t)dst) & wmask) {
			/*
			 * Try to align operands.  This cannot be done
			 * unless the low bits match.
			 */
			if ((t ^ (uintptr_t)dst) & wmask || length < wsize)
				t = length;
			else
				t = wsize - (t & wmask);
			length -= t;
			TLOOP1(*dst++ = *src++);
		}

#if defined (MRV55)
		/* Copy dqword */
		if (length > dqwsize) {
			/* Copy dqword */
			t = length - (length & dqwmask);
			dqword_copy(dst, src, t);
			dst += t;
			src += t;
			length -= t;
		}
#endif

		/* Copy word */
		t = length / wsize;
		TLOOP(*(word *)dst = *(word *)src; src += wsize; dst += wsize);

		/* Copy byte */
		t = length & wmask;
		TLOOP(*dst++ = *src++);
	} else {
		/*
		 * Copy backwards.  Otherwise essentially the same.
		 * Alignment works as before, except that it takes
		 * (t&wmask) bytes to align, not wsize-(t&wmask).
		 */
		src += length;
		dst += length;
		t = (uintptr_t)src;
		if ((t | (uintptr_t)dst) & wmask) {
			if ((t ^ (uintptr_t)dst) & wmask || length <= wsize)
				t = length;
			else
				t &= wmask;
			length -= t;
			TLOOP1(*--dst = *--src);
		}

#if defined (MRV55)
		/* Copy dqword */
		if (length > dqwsize) {
			t = length - (length & dqwmask);
			dqword_copy(dst, src, t);
			dst -= t;
			src -= t;
			length -= t;
		}
#endif

		/* Copy word */
		t = length / wsize;
		TLOOP(src -= wsize; dst -= wsize; *(word *)dst = *(word *)src);

		/* Copy byte */
		t = length & wmask;
		TLOOP(*--dst = *--src);
	}
done:
	return (dst0);
}
#pragma GCC diagnostic pop   // require GCC 4.6

#define	RETURN	return (dst0)
#define	VAL	c0
#define	WIDEVAL	c

#pragma GCC diagnostic push  // require GCC 4.6
#pragma GCC diagnostic ignored "-Wcast-align"
void *memset(void *dst0, int c0, size_t length)
{
	size_t t;
	unsigned int c;
	unsigned char *dst;

	dst = dst0;
	/*
	 * If not enough words, just fill bytes.  A length >= 2 words
	 * guarantees that at least one of them is `complete' after
	 * any necessary alignment.  For instance:
	 *
	 *	|-----------|-----------|-----------|
	 *	|00|01|02|03|04|05|06|07|08|09|0A|00|
	 *	          ^---------------------^
	 *		 dst		 dst+length-1
	 *
	 * but we use a minimum of 3 here since the overhead of the code
	 * to do word writes is substantial.
	 */
	if (length < 3 * wsize) {
		while (length != 0) {
			*dst++ = VAL;
			--length;
		}
		RETURN;
	}

	if ((c = (char)c0) != 0) {	/* Fill the word. */
		c = (c << 8) | c;	/* u_int is 16 bits. */
		c = (c << 16) | c;	/* u_int is 32 bits. */
	}
	/* Align destination by filling in bytes. */
	if ((t = (long)dst & wmask) != 0) {
		t = wsize - t;
		length -= t;
		do {
			*dst++ = VAL;
		} while (--t != 0);
	}

	/* Fill words.  Length was >= 2*words so we know t >= 1 here. */
	t = length / wsize;
	#pragma unroll 2
	do {
		*(unsigned int *)dst = WIDEVAL;
		dst += wsize;
	} while (--t != 0);

	/* Mop up trailing bytes, if any. */
	t = length & wmask;
	if (t != 0)
		do {
			*dst++ = VAL;
		} while (--t != 0);
	RETURN;
}
#pragma GCC diagnostic pop   // require GCC 4.6

void *memmove(void *dest, const void *src, size_t n)
{
  if (dest != src)
    memcpy(dest, src, n);

  return dest;
}

size_t strlen(const char *s)
{
  const char *p = s;
  while (*p)
    p++;
  return p - s;
}

int strcmp(const char* s1, const char* s2)
{
  unsigned char c1, c2;

  do {
    c1 = *s1++;
    c2 = *s2++;
  } while (c1 != 0 && c1 == c2);

  return c1 - c2;
}

char* strcpy(char* dest, const char* src)
{
  char* d = dest;
  while ((*d++ = *src++))
    ;
  return dest;
}

#pragma GCC diagnostic push  // require GCC 4.6
#pragma GCC diagnostic ignored "-Wmissing-prototypes"
long atol(const char* str)
{
  long res = 0;
  int sign = 0;

  while (*str == ' ')
    str++;

  if (*str == '-' || *str == '+') {
    sign = *str == '-';
    str++;
  }

  while (*str) {
    res *= 10;
    res += *str++ - '0';
  }

  return sign ? -res : res;
}
#pragma GCC diagnostic pop   // require GCC 4.6

#if defined (MRV55)
/* Note the semantics is not the same as that of memcpy.
 * If (dst > src), dst and src point to buffer end
 * If (dst < src), dst and src point to buffer start
 */
__attribute__ ((noinline))
static void *dqword_copy(void *dst, const void *src, size_t length)
{
  __asm volatile (
    "    srli a6, %2, 5      \n"  /* a6 := length/32 (number of 8 words) */
    "    bgtu %0, %1, 2f     \n"  /* Copy barkward if (dst >= src) */
    "    mv a7, %0           \n"  /* Copy forward */
    "    do a6, 1f           \n"
    "    lqw a2, (%1)16      \n"
    "    lqw t3, (%1)16      \n"
    "    sqw a2, (a7)16      \n"
    " 1:                     \n"
    "    sqw t3, (a7)16      \n"
    "    ret                 \n"
    "                        \n"
    " 2:                     \n"
    "    addi %1, %1, -16    \n"  /* Copy backward */
    "    addi a7, %0, -16    \n"
    "    do a6, 3f           \n"
    "    lqw a2, (%1)-16     \n"
    "    lqw t3, (%1)-16     \n"
    "    sqw a2, (a7)-16     \n"
    " 3:                     \n"
    "    sqw t3, (a7)-16     \n"
    :
    : "r" (dst), "r"(src), "r"(length)
    : );

    return dst;
}
#endif
