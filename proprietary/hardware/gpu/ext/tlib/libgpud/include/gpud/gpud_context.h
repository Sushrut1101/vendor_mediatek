/*
 * Copyright (C) 2018-2019 MediaTek Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef GPUD_INCLUDE_GPUD_GPUD_CONTEXT_H_
#define GPUD_INCLUDE_GPUD_GPUD_CONTEXT_H_

#include <sys/cdefs.h>
#include <sys/types.h>
#include <dpframework/mmdump.h>
#include <dpframework/mmdump_fmt.h>

__BEGIN_DECLS

typedef enum {
    GPUD_INIT_TYPE_UNKNOWN,
    GPUD_INIT_TYPE_GL,
    GPUD_INIT_TYPE_VK,
    GPUD_INIT_TYPE_CL,
    GPUD_INIT_TYPE_GPUD,
    GPUD_INIT_TYPE_NUM,
} gpud_init_type;

/**
 * @brief Identifies what kind of dumping is requested.
 */
typedef enum {
    GPUD_DUMP_SPECIFIER_TEXIMAGE_READ,
    GPUD_DUMP_SPECIFIER_TEXIMAGE_DRAW,
    GPUD_DUMP_SPECIFIER_AUXIMAGE_READ,
    GPUD_DUMP_SPECIFIER_AUXIMAGE_DRAW,
    GPUD_DUMP_SPECIFIER_GL_EXTIMAGE_READ,
    GPUD_DUMP_SPECIFIER_GL_EXTIMAGE_DRAW,
    GPUD_DUMP_SPECIFIER_VK_EXTIMAGE_READ,
    GPUD_DUMP_SPECIFIER_VK_EXTIMAGE_DRAW,
    GPUD_DUMP_SPECIFIER_GL_ANWBUFFER_READ,
    GPUD_DUMP_SPECIFIER_GL_ANWBUFFER_DRAW,
    GPUD_DUMP_SPECIFIER_VK_ANWBUFFER_READ,
    GPUD_DUMP_SPECIFIER_VK_ANWBUFFER_DRAW,
    GPUD_DUMP_SPECIFIER_SF_ANWBUFFER_READ,
    GPUD_DUMP_SPECIFIER_SF_ANWBUFFER_DRAW,
    GPUD_DUMP_SPECIFIER_SF_SCREENSHOT,
    GPUD_DUMP_SPECIFIER_GL_SHADER_SOURCE,
    GPUD_DUMP_SPECIFIER_VK_SHADER_SOURCE,
    GPUD_DUMP_SPECIFIER_GL_MAP_BUFFER,
    GPUD_DUMP_SPECIFIER_VK_MAP_MEMORY,
    GPUD_DUMP_SPECIFIER_NUM,
} gpud_dump_specifier;

typedef enum {
    GPUD_AUXIMAGE_REQUIREMENT_UNKNOWN,
    GPUD_AUXIMAGE_REQUIREMENT_VIDEO,
    GPUD_AUXIMAGE_REQUIREMENT_UIPQ,
    GPUD_AUXIMAGE_REQUIREMENT_NUM,
} gpud_auximage_requirement;

typedef enum {
    GPUD_AUXIMAGE_COLORSPACE_BT601,
    GPUD_AUXIMAGE_COLORSPACE_BT709,
    GPUD_AUXIMAGE_COLORSPACE_JPEG,
    GPUD_AUXIMAGE_COLORSPACE_FULL_BT601 = GPUD_AUXIMAGE_COLORSPACE_JPEG,
    GPUD_AUXIMAGE_COLORSPACE_BT2020,
    GPUD_AUXIMAGE_COLORSPACE_FULL_BT709,
    GPUD_AUXIMAGE_COLORSPACE_FULL_BT2020,
} gpud_auximage_colorspace;

typedef struct AUXDpPqParamInfo {
    int scenario;
    unsigned int enable;
    unsigned int video_paramTable;
    unsigned int video_id;
    unsigned int video_timeStamp;
    bool video_isHDR2SDR;
    unsigned int scltmEnable;
    unsigned int scltmPosition;
    unsigned int scltmOrientation;
    unsigned int scltmTableIndex;
} AUXDpPqParamInfo;

/*
 * mdp_width/mdp_height:
 *     width need do alignment if it have width_align
 *     for blk/ufo format, mdp need the height with height_align
 *     for other format, mdp need original source height
 */
typedef struct AUXImageInfo {
    int specifier;
    int ion_fd;
    int format;
    int DPFormat;
    int width; // the width after width_align
    int height; // the height after height_align
    bool is_omx_align;
    int uv_align;
    int width_align;
    int height_align;
    int stride;
    int y_stride;
    int uv_stride;
    unsigned int size;
    unsigned int yuv_info;
    unsigned int yuv_info_orig;
    int mdp_width; // the width mdp need
    int mdp_height; // the height mdp need
} AUXImageInfo;

typedef struct TexImageInfo {
    bool compressed;
    unsigned int target;
    int level;
    int internalformat;
    int xoffset;
    int yoffset;
    int zoffset;
    int width;
    int height;
    int depth;
    unsigned int format;
    unsigned int type;
    size_t size;
} TexImageInfo;

typedef struct ANWBufferInfo {
    int specifier;
    unsigned int format;
    int width;
    int height;
    int stride;
    size_t size;
} ANWBufferInfo;

typedef struct FramebufferInfo {
    int read_binding;
    int draw_binding;
    unsigned int target;
    unsigned int format;
    unsigned int type;
    int x;
    int y;
    int width;
    int height;
    size_t size;
} FramebufferInfo;

typedef struct ShaderSourceInfo {
    unsigned int id;
    uint64_t hashcode;
    char *string;
    size_t size;
} ShaderSourceInfo;

typedef struct MapInfo {
    int specifier;
    uint64_t offset;
    uint64_t size;
    uint64_t flags;
    uint32_t target;
    uint32_t access;
} MapInfo;

typedef struct BufferExtraInfo {
    uint64_t id;
    int share_fd;
    int width;
    int height;
    int format;
    int req_format;
    int stride;
    int vertical_stride;
    int stride_2nd;
    int vertical_stride_2nd;
    int alloc_size;
    int priv_flags;
    uint64_t consumer_usage;
    uint64_t producer_usage;
    uint64_t alloc_format;
    bool is_compressed;
    int32_t sf_status;
    int32_t sf_status2;
    uint32_t timestamp;
    unsigned int yuv_info;
} BufferExtraInfo;

typedef struct gpud_gl_context {
    char vendor[64];
    char renderer[64];
    int viewport_x;  // valid range : > 0
    int viewport_y;  // valid range : > 0
    int viewport_width;  // valid range : > 0
    int viewport_height;  // valid range : > 0
    int scissor_x;  // valid range : > 0
    int scissor_y;  // valid range : > 0
    int scissor_width;  // valid range : > 0
    int scissor_height;  // valid range : > 0
    char polygonoffset_factor[64];
    char polygonoffset_units[64];
    char clear[64];
    float clear_color_r;  // valid range : 0.0 ~ 1.0
    float clear_color_g;  // valid range : 0.0 ~ 1.0
    float clear_color_b;  // valid range : 0.0 ~ 1.0
    float clear_color_a;  // valid range : 0.0 ~ 1.0
    float clear_depthf;  // valid range : 0.0 ~ 1.0
    char clear_stencil[64];
    char tex_parameteri_target[64];
    char tex_parameteri_pname[64];
    char tex_parameteri_param[64];
    char tex_parameterf_target[64];
    char tex_parameterf_pname[64];
    char tex_parameterf_param[64];
    char sampler_parameteri_pname[64];
    char sampler_parameteri_param[64];
    char sampler_parameterf_pname[64];
    char sampler_parameterf_param[64];
    bool state_error_dump;
    bool readpixels_dump;
    bool mapbuffer_dump;
    bool bindframebuffer_dump;
    bool shadersource_hack;
    bool shadersource_dump;
    char shadersource_hack_oldsubstr[256];
    char shadersource_hack_newsubstr[256];
    char shadersource_hack_oldsubstr_2[256];
    char shadersource_hack_newsubstr_2[256];
    char shadersource_hack_hashcode[256];
    bool force_rgb8888;
} gpud_gl_context;

typedef struct gpud_egl_context {
    bool choose_config;
    char attr_config_id[64];
    char attr_buffer_size[64];
    char attr_red_size[64];
    char attr_green_size[64];
    char attr_blue_size[64];
    char attr_luminance_size[64];
    char attr_alpha_size[64];
    char attr_alpha_mask_size[64];
    char attr_bind_to_texture_rgb[64];
    char attr_bind_to_texture_rgba[64];
    char attr_color_buffer_type[64];
    char attr_config_caveat[64];
    char attr_conformant[64];
    char attr_depth_size[64];
    char attr_level[64];
    char attr_match_native_pixmap[64];
    char attr_max_swap_interval[64];
    char attr_min_swap_interval[64];
    char attr_native_renderable[64];
    char attr_native_visual_type[64];
    char attr_renderable_type[64];
    char attr_sample_buffers[64];
    char attr_samples[64];
    char attr_stencil_size[64];
    char attr_surface_type[64];
    char attr_transparent_type[64];
    char attr_transparent_red_value[64];
    char attr_transparent_green_value[64];
    char attr_transparent_blue_value[64];
    char attr_recordable_android[64];
    char attr_color_component_type_ext[64];
    char attr_yuv_order_ext[64];
    char attr_yuv_number_of_planes_ext[64];
    char attr_yuv_subsample_ext[64];
    char attr_yuv_depth_range_ext[64];
    char attr_yuv_csc_standard_ext[64];
    char attr_yuv_plane_bpp_ext[64];
} gpud_egl_context;

typedef struct gpud_vk_context {
    int viewport_x;
    int viewport_y;
    int viewport_width;
    int viewport_height;
    bool mapmemory_dump;
    char instance_returncode[64];
    char physicaldevice_vendor_id[64];
    char physicaldevice_devicename[64];
} gpud_vk_context;

typedef struct gpud_fwrite_context {
    bool is_enabled;
    bool rgba1010102;
} gpud_fwrite_context;

typedef struct gpud_mmdump_context {
    bool is_debugging;
    void *handle;
    mmdump_func fnDump;
    mmdump2_func fnDump2;
} gpud_mmdump_context;

typedef struct gpud_mmpath_context {
    bool is_debugging;
} gpud_mmpath_context;

typedef struct gpud_context {
    bool is_debugging;
    int is_initialized;
    bool is_logging;
    bool is_gl_logging;
    bool is_vk_logging;
    bool is_aux_logging;
    bool is_gralloc_logging;
    uint16_t fbc_disable_pattern;
    int pid;
    int tid;
    char pname[256];
    char dump_pname[256];
    char folder[64];
    char auximage_colorspace[64];
    int auximage_dstformat;  // valid range : > 0
    int auximage_supportformat;  // valid range : > 0
    bool auximage_dump;
    bool extimage_dump;
    bool teximage_dump;
    bool wsframebuffer_dump;
    bool wsframebuffer_log;
	bool screenshot_dump;
    bool extimage_drawblock;
    bool wsframebuffer_drawline;
    int wsframebuffer_drawline_width;
    gpud_gl_context gl;
    gpud_egl_context egl;
    gpud_vk_context vk;
    gpud_fwrite_context fwrite;
    gpud_mmdump_context mmdump;
    gpud_mmpath_context mmpath;
} gpud_context;

extern gpud_context g_gpud_context;

__END_DECLS

#endif  // GPUD_INCLUDE_GPUD_GPUD_CONTEXT_H_
