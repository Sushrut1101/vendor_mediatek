/*
 * FreeRTOS Kernel V10.0.0
 * Copyright (C) 2017 Amazon.com, Inc. or its affiliates.  All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software. If you wish to use our Amazon
 * FreeRTOS name, please do so in a fair use way that does not cause confusion.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * http://www.FreeRTOS.org
 * http://aws.amazon.com/freertos
 *
 * 1 tab == 4 spaces!
 */


#ifndef PORTMACRO_H
#define PORTMACRO_H

#ifdef __cplusplus
extern "C" {
#endif

/*-----------------------------------------------------------
 * Port specific definitions.
 *
 * The settings in this file configure FreeRTOS correctly for the
 * given hardware and compiler.
 *
 * These settings should not be altered.
 *-----------------------------------------------------------
 */
#ifndef traceISR_ENTER
        #define traceISR_ENTER()
#endif

#ifndef traceISR_EXIT
        #define traceISR_EXIT()
#endif

#ifndef traceINT_OFF
#define traceINT_OFF()
#define traceINT_OFF_FROM_ISR()
#endif

#ifndef traceINT_ON
#define traceINT_ON()
#define traceINT_ON_FROM_ISR()
#endif

/* Type definitions. */
#define portCHAR		char
#define portFLOAT		float
#define portDOUBLE		double
#define portLONG		long
#define portSHORT		short
#define portBASE_TYPE	long

#if __riscv_xlen == 64
	#define portSTACK_TYPE	uint64_t
	#define portPOINTER_SIZE_TYPE	uint64_t
#else
	#define portSTACK_TYPE	uint32_t
	#define portPOINTER_SIZE_TYPE	uint32_t
#endif

typedef portSTACK_TYPE StackType_t;
typedef long BaseType_t;
typedef unsigned long UBaseType_t;

#if( configUSE_16_BIT_TICKS == 1 )
	typedef uint16_t TickType_t;
	#define portMAX_DELAY ( TickType_t ) 0xffff
#else
	typedef uint32_t TickType_t;
	#define portMAX_DELAY ( TickType_t ) 0xffffffffUL
#endif
/*-----------------------------------------------------------*/

/* Architecture specifics. */
#define portSTACK_GROWTH			( -1 )
#define portTICK_PERIOD_MS			( ( TickType_t ) (1000 / configTICK_RATE_HZ) )

/* for newlib floating point printing, need 8-byte alignment to convert to double precision floating temporalily */
#define portBYTE_ALIGNMENT		16
#if 0
#if __riscv_xlen == 64
	#define portBYTE_ALIGNMENT	8
#else
	#define portBYTE_ALIGNMENT	4
#endif
#endif
#define portCRITICAL_NESTING_IN_TCB					1
/*-----------------------------------------------------------*/
#include "syscalls.h"
/* Scheduler utilities. */
extern void vPortYield( void );

#include <encoding.h>
#ifdef configMultiHarts
#define portTICK_TYPE_IS_ATOMIC 1
#define portNUM_CPUS        configTOTAL_NUM_CPUS      /* total harts */
#define SWI_EVENT_NONE         0
#define SWI_EVENT_WAKE_UP      1
#define SWI_EVENT_ADDNEW_TCB   2
void vPortCPUSWIHandler( void );
void vPortCPUSleep( void );
void vPortCPUWakeup( int cpuid );
void vPortAddNewTaskToCpuReadyList(int cpuid , void* pxTCB);
void vPortCPURequestSwitch( int cpuid );
void vPortEnterCritical( void );
void vPortExitCritical( void );
void vPortClearInterruptMask_SMP( int );
int vPortSetInterruptMask_SMP( void );

#if defined(P_MODE_0) // task/RTOS in machine mode
#define portYIELD()	vPortCPURequestSwitch(uxPortGetCpuId())
#elif defined(P_MODE_1) // task/RTOS in user mode
#define portYIELD()	syscall(SYS_yield, 0, 0, 0)
#endif /* P_MODE_0 */

#define vPortYieldFromISR() vPortCPURequestSwitch(uxPortGetCpuId())

#define portENTER_CRITICAL()					vPortEnterCritical()
#define portEXIT_CRITICAL()					vPortExitCritical()
#define portEND_SWITCHING_ISR( xSwitchRequired ) if( xSwitchRequired >= 0 ) vPortCPURequestSwitch(xSwitchRequired)
#define portYIELD_FROM_ISR( x ) portEND_SWITCHING_ISR( x )
#define uxPortGetCpuId()					mrv_get_cpuid()

#define portASSERT_IF_IN_ISR() { extern int IRQ[portNUM_CPUS]; unsigned long hartid = read_csr(mhartid); if(IRQ[hartid] > 0) platform_assert("Enter critical section in an ISR", __FILE__, __LINE__); }

#define portSET_INTERRUPT_MASK_FROM_ISR()       vPortSetInterruptMask_SMP()
#define portCLEAR_INTERRUPT_MASK_FROM_ISR( uxSavedStatusValue )       vPortClearInterruptMask_SMP( uxSavedStatusValue )

#else
/* not configMultiHarts */
//#define portYIELD()	vPortYield()
#if defined(P_MODE_0) // task/RTOS in machine mode
//MRV_VIC_MIASWI_L  (0x5c0)
#define portYIELD() {                                                    \
                        volatile char hartid;                            \
                        __asm volatile("csrr %0, mhartid":"=r"(hartid)::); \
                        hartid = 1 << hartid;                            \
                        __asm volatile("csrw 0x5c0, %0"::"r"(hartid):);}
#elif defined(P_MODE_1) // task/RTOS in user mode
#define portYIELD()	syscall(SYS_yield, 0, 0, 0)
#endif /* P_MODE_0 */

#define vPortYieldFromISR() {                                                    \
                                volatile char hartid;                            \
                                __asm volatile("csrr %0, mhartid":"=r"(hartid)::); \
                                hartid = 1 << hartid;                            \
                                __asm volatile("csrw 0x5c0, %0"::"r"(hartid):);}
#define portENTER_CRITICAL()					vTaskEnterCritical()
#define portEXIT_CRITICAL()					vTaskExitCritical()
#define portEND_SWITCHING_ISR( xSwitchRequired ) if( xSwitchRequired ) vPortYieldFromISR()
#define portYIELD_FROM_ISR( x ) portEND_SWITCHING_ISR( x )

#define portASSERT_IF_IN_ISR() { extern int IRQ;  if(IRQ > 0) platform_assert("Enter critical section in an ISR", __FILE__, __LINE__); }

#define portSET_INTERRUPT_MASK_FROM_ISR()       vPortSetInterruptMask()
#define portCLEAR_INTERRUPT_MASK_FROM_ISR( uxSavedStatusValue )       vPortClearInterruptMask( uxSavedStatusValue )

#endif /* configMultiHarts */
/*-----------------------------------------------------------*/


/* Critical section management. */
extern int vPortSetInterruptMask( void );
extern void vPortClearInterruptMask( int );
extern void vTaskEnterCritical( void );
extern void vTaskExitCritical( void );

#if defined(P_MODE_0) // task/RTOS in machine mode
#define portDISABLE_INTERRUPTS()				__asm volatile  ( "csrci mstatus,8" )
#define portENABLE_INTERRUPTS()					__asm volatile  ( "csrsi mstatus,8" )
#elif defined(P_MODE_1) // task/RTOS in user mode
#define portDISABLE_INTERRUPTS()				syscall(SYS_disable_int, 0, 0, 0)
#define portENABLE_INTERRUPTS()					syscall(SYS_enable_int, 0, 0, 0)
#endif /* P_MODE_0 */

/*-----------------------------------------------------------*/

/* Task function macros as described on the FreeRTOS.org WEB site. */
#define portTASK_FUNCTION_PROTO( vFunction, pvParameters ) void vFunction( void *pvParameters )
#define portTASK_FUNCTION( vFunction, pvParameters ) void vFunction( void *pvParameters )

#define portNOP() __asm volatile 	( " nop;nop;nop;nop;nop;nop;nop;nop " )

#ifndef traceTASK_SWITCHED_IN
#ifdef MRV_PROFILING_HOOK
void MrvProfilingTaskSwtich(void);
#define traceTASK_SWITCHED_IN() MrvProfilingTaskSwtich()
#endif /* MRV_PROFILING_HOOK */
#endif /* traceTASK_SWITCHED_IN */

#ifdef __cplusplus
}
#endif

#endif /* PORTMACRO_H */

