/*
 * Copyright (C) 2018-2019 MediaTek Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef GPUD_INCLUDE_GPUD_GPUD_GRALLOC_H_
#define GPUD_INCLUDE_GPUD_GPUD_GRALLOC_H_

#include <hardware/gralloc.h>
#include <hardware/gralloc1.h>
#include <utils/Singleton.h>
#include <nativebase/nativebase.h>
#include <android/hardware/graphics/common/1.2/types.h>

using namespace android;
using android::hardware::graphics::common::V1_2::BufferUsage;

#define GPUD_GRALLOC_USAGE_PRIVATE_MASK      (0xffff0000f0000000U)
#define GPUD_GRALLOC_USAGE_SW_READ_RARELY    static_cast<uint64_t>(BufferUsage::CPU_READ_RARELY)
#define GPUD_GRALLOC_USAGE_SW_READ_OFTEN     static_cast<uint64_t>(BufferUsage::CPU_READ_OFTEN)
#define GPUD_GRALLOC_USAGE_SW_READ_MASK      static_cast<uint64_t>(BufferUsage::CPU_READ_MASK)
#define GPUD_GRALLOC_USAGE_SW_WRITE_RARELY   static_cast<uint64_t>(BufferUsage::CPU_WRITE_RARELY)
#define GPUD_GRALLOC_USAGE_SW_WRITE_OFTEN    static_cast<uint64_t>(BufferUsage::CPU_WRITE_OFTEN)
#define GPUD_GRALLOC_USAGE_SW_WRITE_MASK     static_cast<uint64_t>(BufferUsage::CPU_WRITE_MASK)
#define GPUD_GRALLOC_USAGE_HW_TEXTURE        static_cast<uint64_t>(BufferUsage::GPU_TEXTURE)
#define GPUD_GRALLOC_USAGE_HW_RENDER         static_cast<uint64_t>(BufferUsage::GPU_RENDER_TARGET)
#define GPUD_GRALLOC_USAGE_HW_COMPOSER       static_cast<uint64_t>(BufferUsage::COMPOSER_OVERLAY)
#define GPUD_GRALLOC_USAGE_HW_FB             static_cast<uint64_t>(BufferUsage::COMPOSER_CLIENT_TARGET)
#define GPUD_GRALLOC_USAGE_HW_VIDEO_ENCODER  static_cast<uint64_t>(BufferUsage::VIDEO_ENCODER)
#define GPUD_GRALLOC_USAGE_VIDEO_DECODER     static_cast<uint64_t>(BufferUsage::VIDEO_DECODER)
#define GPUD_GRALLOC_USAGE_HW_CAMERA_READ    static_cast<uint64_t>(BufferUsage::CAMERA_INPUT)
#define GPUD_GRALLOC_USAGE_HW_CAMERA_WRITE   static_cast<uint64_t>(BufferUsage::CAMERA_OUTPUT)
#define GPUD_GRALLOC_USAGE_PROTECTED         static_cast<uint64_t>(BufferUsage::PROTECTED)

class GrallocHalWrapper : public Singleton<GrallocHalWrapper> {
 public:
    int registerBuffer(const ANativeWindowBuffer *buffer);
    int unregisterBuffer(const ANativeWindowBuffer *buffer);
    int lockBuffer(const ANativeWindowBuffer *buffer, int usage, void **vaddr);
    int unlockBuffer(const ANativeWindowBuffer *buffer);

 private:
    friend class Singleton<GrallocHalWrapper>;
    GrallocHalWrapper();
    ~GrallocHalWrapper();
    static int mapGralloc1Error(int grallocError);
    static buffer_handle_t getBufferHandle(const ANativeWindowBuffer *buffer);

    int mError;
    int mVersion;
    gralloc_module_t *mGrallocModule;
    // gralloc
    alloc_device_t *mAllocDevice;
    // gralloc1
    gralloc1_device_t *mGralloc1Device;
    GRALLOC1_PFN_RETAIN mPfnRetain;
    GRALLOC1_PFN_RELEASE mPfnRelease;
    GRALLOC1_PFN_GET_NUM_FLEX_PLANES mPfnGetNumFlexPlanes;
    GRALLOC1_PFN_LOCK mPfnLock;
    GRALLOC1_PFN_LOCK_FLEX mPfnLockFlex;
    GRALLOC1_PFN_UNLOCK mPfnUnlock;
};

#endif  // GPUD_INCLUDE_GPUD_GPUD_GRALLOC_H_
