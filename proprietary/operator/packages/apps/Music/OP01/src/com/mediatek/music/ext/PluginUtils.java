/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2016. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */
package com.mediatek.music.ext;

import android.content.Context;

/**
 * The Utils that is used by plug-in to call host.
 */
public class PluginUtils {
    /**
     * Base MENU ITEM ID that is used to extend the menu.
     */
    public static final int MENU_ITEM_BASE_ID = 0xF0000000;


    /**
     * Base Activity Request Code.
     */

    public static final int ACTIVITY_REQUEST_CODE_BASE_ID = 1000;

    /**
     * The name of the activity whose menu will be customized.
     */

    public static final String TRACK_BROWSER_ACTIVITY = "TrackBrowserActivity";
    public static final String MUSIC_BROWSER_ACTIVITY = "MusicBrowserActivity";
    public static final String PLAYLIST_BROWSER_ACTIVITY = "PlaylistBrowserActivity";


    /**
     * Tab Index value.
     */
    public static final int ARTIST_TAB_INDEX = 0;
    public static final int ALBUM_TAB_INDEX = 1;
    public static final int SONG_TAB_INDEX = 2;
    public static final int PLAYLIST_TAB_INDEX = 3;


    /**
     * keys of Bundle which will be passed as the parameter when the following interface is called.
     * by host when the TrackBrowserActivity is used to display the songs of a playlist
     *   onCreateOptionsMenuForPlugin
     *   onPrepareOptionsMenuForPlugin
     *   onOptionsItemSelectedForPlugin
     *   onActivityResultForPlugin
     *
     */

    public static final String PLAYLIST_NAME = "playlistname";
    public static final String PLAYLIST_LEN = "playlistlen";


    /**
     * keys of Bundle which will be passed as the parameter when the following interface is called.
     * by host when MusicBrowserActivity is used to manager the artist/album/song/playlist
     *   onCreateOptionsMenuForPlugin
     *   onPrepareOptionsMenuForPlugin
     *   onOptionsItemSelectedForPlugin
     */
    public static final String TAB_INDEX = "tabindex";


    /**
     * pre defined playlist name.
     */

    public static final String NOW_PLAYING = "nowplaying";
    public static final String RECENTLY_ADDED = "recentlyadded";
    public static final String PODCASTS = "podcasts";

    /**
     * The interface for plug-in to call host method.
     */
    public interface IMusicListenter {
        /**
         * to play the playlist.
         *
         * @param context the application context
         * @param list the list to play
         * @param position the start play position
         *
         * @internal
         */
        public void onCallPlay(Context context, long [] list, int position);

        /**
         * to add to the specific playlist.
         *
         * @param context the application context
         * @param ids the list to add
         * @param playlistid the playlist to add to
         *
         * @internal
         */
        public void onCallAddToPlaylist(Context context, long [] ids, long playlistid);

        /**
         * to add to the current playlist.
         *
         * @param context the application context
         * @param list the list to add
         *
         * @internal
         */
        public void onCallAddToCurrentPlaylist(Context context, long [] list);

        /**
         * to clear playlist.
         *
         * @param context the application context
         * @param playlistid the playlist id
         *
         * @internal
         */
        public void onCallClearPlaylist(Context context, int playlistid);
   }
}