LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl liblog libm libnvram libutils libmtk_drvb
LOCAL_CHECK_ELF_FILES := false
ifeq ($(MTK_GAUGE_VERSION), 30)
	LOCAL_PROPRIETARY_MODULE := true
	LOCAL_MODULE_OWNER := mtk
	LOCAL_MODULE_CLASS := SHARED_LIBRARIES
	LOCAL_MODULE := libfgauge_gm30
ifeq ($(MTK_GENERIC_HAL), yes)
	LOCAL_SRC_FILES_arm := GKI20/libfgauge_gm30.so
	LOCAL_SRC_FILES_arm64 := GKI20/libfgauge_gm30_64.so
	LOCAL_MULTILIB := both
else
ifeq ($(LINUX_KERNEL_VERSION), kernel-4.14)
	LOCAL_SRC_FILES_arm := kernel-4.14/libfgauge_gm30.so
	LOCAL_MULTILIB := 32
endif
ifeq ($(LINUX_KERNEL_VERSION), kernel-4.19)
ifneq (,$(filter $(strip $(MTK_PLATFORM_DIR)), mt6853 mt6768 mt6739 mt6785 mt6885 mt6873 mt6893 mt6771))
	LOCAL_SRC_FILES_arm := kernel-4.14/libfgauge_gm30.so
else
	LOCAL_SRC_FILES_arm := kernel-4.19/libfgauge_gm30.so
endif
	LOCAL_MULTILIB := 32
endif
ifeq ($(LINUX_KERNEL_VERSION), kernel-4.19-lc)
	LOCAL_SRC_FILES_arm := kernel-4.19/libfgauge_gm30.so
	LOCAL_MULTILIB := 32
endif
ifeq ($(LINUX_KERNEL_VERSION), kernel-5.4)
	LOCAL_SRC_FILES_arm := kernel-5.4/libfgauge_gm30.so
	LOCAL_MULTILIB := 32
endif
ifeq ($(LINUX_KERNEL_VERSION), kernel-5.10)
	LOCAL_SRC_FILES_arm := kernel-5.10/libfgauge_gm30.so
	LOCAL_SRC_FILES_arm64 := kernel-5.10/libfgauge_gm30_64.so
	LOCAL_MULTILIB := both
endif
ifeq ($(LINUX_KERNEL_VERSION), kernel-mainline)
	LOCAL_SRC_FILES_arm := kernel-5.4/libfgauge_gm30.so
	LOCAL_MULTILIB := 32
endif
endif
	LOCAL_MODULE_SUFFIX := .so
	include $(BUILD_PREBUILT)
else
	LOCAL_PROPRIETARY_MODULE := true
	LOCAL_MODULE_OWNER := mtk
	LOCAL_MODULE_CLASS := SHARED_LIBRARIES
	LOCAL_MODULE := libfgauge
	LOCAL_SRC_FILES_arm := libfgauge.so
	LOCAL_MULTILIB := 32
	LOCAL_MODULE_SUFFIX := .so
	include $(BUILD_PREBUILT)
endif
