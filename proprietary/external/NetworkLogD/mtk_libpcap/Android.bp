package {
    default_applicable_licenses: ["NetworkLogD_mtk_libpcap_license"],
}

license {
    name: "NetworkLogD_mtk_libpcap_license",
    visibility: [":__subpackages__"],
    license_kinds: [
        "SPDX-license-identifier-Apache-2.0",
        "SPDX-license-identifier-BSD",
        "SPDX-license-identifier-ISC",
        "SPDX-license-identifier-MIT",
    ],
    license_text: [
        "LICENSE",
        "NOTICE",
    ],
}

cc_defaults {
    name: "libmtkpcap_defaults",
    cflags: [
        "-D_BSD_SOURCE",
        "-DHAVE_CONFIG_H",
        "-Dlint",
        "-D_U_=__attribute__((__unused__))",
        "-Wall",
        "-Werror",
        "-Wno-macro-redefined",
        "-Wno-pointer-arith",
        "-Wno-sign-compare",
        "-Wno-unused-parameter",
        "-Wno-unused-result",
        "-Wno-tautological-compare",
        "-DMTK_COUNT_FEATURE",
    ],
}

cc_library {
    name: "libpcap_bak",
    host_supported: true,
    vendor_available: true,
    // Build against the NDK 29 because it's used by the network stack mainline module tests, which
    // need to support Q.
    // TODO(b/148792341): stop hardcoding sdk_version integers in libraries all over the tree and
    // define a min_apex_sdk_version property that all module code can use.
    sdk_version: "29",
    defaults: ["libmtkpcap_defaults"],

    // (Matches order in libpcap's Makefile.)
    srcs: [
        "pcap-linux.c",
        "pcap-usb-linux.c",
        "pcap-netfilter-linux-android.c",
        "fad-getad.c",
        "pcap.c",
        "gencode.c",
        "optimize.c",
        "nametoaddr.c",
        "etherent.c",
        "fmtutils.c",
        "savefile.c",
        "sf-pcap.c",
        "sf-pcapng.c",
        "pcap-common.c",
        "bpf_image.c",
        "bpf_filter.c",
        "bpf_dump.c",
        "scanner.c",
        "grammar.c",
    ],
    shared_libs: [
        "liblog",
        "libcutils",
    ],

    target: {
        linux: {
            srcs: [
                "missing/strlcpy.c",
            ],
        },
        darwin: {
            enabled: false,
        },
    },
    system_ext_specific: true,
    export_include_dirs: ["."],
}

//
// Tests.
//

cc_test {
    name: "libpcap_baktest",
    defaults: ["libmtkpcap_defaults"],
    gtest: false,
    // (Matches order in libpcap's Makefile.)
    srcs: [
        "testprogs/can_set_rfmon_test.c",
        "testprogs/capturetest.c",
        "testprogs/filtertest.c",
        "testprogs/findalldevstest.c",
        "testprogs/opentest.c",
        "testprogs/reactivatetest.c",
        "testprogs/selpolltest.c",
        "testprogs/threadsignaltest.c",
        "testprogs/valgrindtest.c",
    ],
    static_libs: ["libpcap_bak"],
    test_per_src: true,
}
