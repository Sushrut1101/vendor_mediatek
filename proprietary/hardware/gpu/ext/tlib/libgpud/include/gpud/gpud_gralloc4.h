/*
 * Copyright (C) 2018-2019 MediaTek Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef GPUD_INCLUDE_GPUD_GPUD_GRALLOC4_H_
#define GPUD_INCLUDE_GPUD_GPUD_GRALLOC4_H_

#include <android/hardware/graphics/common/1.2/types.h>
#include <android/hardware/graphics/mapper/4.0/IMapper.h>
#include <gpud/gpud_gralloc_mapper.h>

using namespace android;

class GPUDGralloc4Mapper : public GPUDGrallocMapper {
 public:
    GPUDGralloc4Mapper();
    bool isLoaded() const override;
    status_t lock(buffer_handle_t bufferHandle, uint64_t usage, int32_t left, int32_t top,
                  int32_t width, int32_t height, int acquireFence, void** outData,
                  int32_t* outBytesPerPixel, int32_t* outBytesPerStride) const override;
    status_t lock(buffer_handle_t bufferHandle, uint64_t usage, int32_t left, int32_t top,
                  int32_t width, int32_t height, int acquireFence, android_ycbcr* ycbcr) const override;
    int unlock(buffer_handle_t bufferHandle) const override;
    status_t getPixelFormatFourCC(buffer_handle_t bufferHandle, uint32_t* outPixelFormatFourCC) const override;
    status_t getPlaneLayouts(buffer_handle_t bufferHandle,
                             std::vector<PlaneLayout>* outPlaneLayouts) const override;
 private:
    template <class T>
    using DecodeFunction = status_t (*)(const hardware::hidl_vec<uint8_t>& input, T* output);
    template <class T>
    status_t get(
        buffer_handle_t bufferHandle,
        const android::hardware::graphics::mapper::V4_0::IMapper::MetadataType& metadataType,
        DecodeFunction<T> decodeFunction, T* outMetadata) const;
    sp<hardware::graphics::mapper::V4_0::IMapper> mMapper;
};

#endif  // GPUD_INCLUDE_GPUD_GPUD_GRALLOC4_H_
