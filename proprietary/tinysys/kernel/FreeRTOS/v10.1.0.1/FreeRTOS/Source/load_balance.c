/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER\'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER\'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER\'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK\'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK\'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver\'s
 * applicable license agreements with MediaTek Inc.
 */

#include <FreeRTOS.h>
#include <task.h>
#include <queue.h>
#include <string.h>
#include <load_balance.h>
#include <mt_printf.h>

#define MAX_LB_QUEUE_EVENT    (2)

#define LDBL_PRINTF(fmt, ...)                                               \
        do {                                                                \
                PRINTF_E("%s " fmt "\n", pcLdblPrompt, ##__VA_ARGS__); \
        } while(0)

static char *pcLdblPrompt = "[LDBL]";
static QueueHandle_t xLoadBalanceRxQueue;

/* A while list to instance the Affinity function.
 * That is the tasks in white list can be migrated otherwise not allowed.
 */
static char *ppcLDBLWhiteList[] = {
	CFG_LDBL_WHILE_LIST_ITEM_1,
	CFG_LDBL_WHILE_LIST_ITEM_2,
	CFG_LDBL_WHILE_LIST_ITEM_3,
	CFG_LDBL_WHILE_LIST_ITEM_4,
	CFG_LDBL_WHILE_LIST_ITEM_5,
};

/*-----------------------------------------------------------*/
static BaseType_t prvLoadBalanceWhiteListCheck( char *pcTaskName )
{
	BaseType_t xWhiteListSize = sizeof(ppcLDBLWhiteList) / sizeof(ppcLDBLWhiteList[0]);
	BaseType_t xIdx;

	for ( xIdx = 0; xIdx < xWhiteListSize; xIdx++ ) {
		if ( strcmp( ppcLDBLWhiteList[xIdx], pcTaskName ) == 0 )
			return pdTRUE;
	}

	return pdFALSE;
}

/*-----------------------------------------------------------*/
static void prvLoadBalanceManager( void *pvParameters )
{
	TaskHandle_t xLoadBalanceHandle;
	UBaseType_t uxCpuAffinity, xFromCpuId, xToCpuId;
	char *pcTaskName;
	struct loadBalanceQueueEvent xEvent = { "", 0, 0 };
	BaseType_t xRet = pdFALSE;

	while (1) {
		xRet = xQueueReceive( xLoadBalanceRxQueue, &xEvent, portMAX_DELAY );
		if (xRet != pdPASS) {
			LDBL_PRINTF("xQueueReceive failed, %ld", xRet);
			continue;
		}

		xFromCpuId = xEvent.xFromCpuId;
		xToCpuId = xEvent.xToCpuId;
		pcTaskName = xEvent.pcTaskName;

		if ( xFromCpuId == xToCpuId ) {
			LDBL_PRINTF( "FromId is equal to ToId, %lu", xFromCpuId );
			continue;
		}

		/* skip the event, if the name is not in the while list */
		if ( prvLoadBalanceWhiteListCheck( pcTaskName ) == pdFALSE ) {
			LDBL_PRINTF( "task, %s, not in white list\n", pcTaskName );
			continue;
		}

		xLoadBalanceHandle = xTaskGetHandle( pcTaskName );
		if ( xLoadBalanceHandle == NULL ) {
			LDBL_PRINTF( "cannot find task, %s", pcTaskName );
			continue;
		}

		uxCpuAffinity = xTaskGetAffinity( xLoadBalanceHandle );
		if ( uxCpuAffinity == xToCpuId ) {
			LDBL_PRINTF( "task, %s, already in cpu-%lu", pcTaskName, xToCpuId );
			continue;
		}

		do {
			xRet = xLoadBalanceMigration(
				xFromCpuId,
				xToCpuId,
				xLoadBalanceHandle );

			LDBL_PRINTF( "migrate task, %s to cpu-%lu, %ld", pcTaskName, xToCpuId, xRet );

			/* if migration fails, wait for a moment for migrated task to finish
			 * its current job and then try again.
			 */
			if ( xRet != pdTRUE )
				vTaskDelay(1);

		} while (xRet != pdTRUE);
	}
}

/*-----------------------------------------------------------*/
void vLoadBalanceEventSendFromISR(BaseType_t xFromCpuId, BaseType_t xToCpuId, char *pcTaskName)
{
	struct loadBalanceQueueEvent xEvent;
	BaseType_t xHigherPriorityTaskWoken;
	BaseType_t xRet = pdPASS;

	xEvent.xFromCpuId = xFromCpuId;
	xEvent.xToCpuId = xToCpuId;
	memset(xEvent.pcTaskName, 0, configMAX_TASK_NAME_LEN);
	strncpy(xEvent.pcTaskName, pcTaskName, configMAX_TASK_NAME_LEN-1);

	xRet =
	    xQueueSendFromISR( xLoadBalanceRxQueue, &xEvent,
			      &xHigherPriorityTaskWoken );
	configASSERT(xRet == pdTRUE);

	/* context switching to load balance manager */
	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

/*-----------------------------------------------------------*/
BaseType_t xLoadBalanceInit( void )
{
	BaseType_t xRet = pdPASS;

	xLoadBalanceRxQueue = xQueueCreate(MAX_LB_QUEUE_EVENT,
			 sizeof(struct loadBalanceQueueEvent));

	xRet = xTaskCreate( prvLoadBalanceManager, "LDBL",
			configMINIMAL_STACK_SIZE, NULL, PRI_LDBL, NULL );

	return xRet;
}
