/*
 * FreeRTOS Kernel V10.0.0
 * Copyright (C) 2017 Amazon.com, Inc. or its affiliates.  All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software. If you wish to use our Amazon
 * FreeRTOS name, please do so in a fair use way that does not cause confusion.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * http://www.FreeRTOS.org
 * http://aws.amazon.com/freertos
 *
 * 1 tab == 4 spaces!
 */

/*-----------------------------------------------------------
 * Implementation of functions defined in portable.h for the RISC-V port.
 *----------------------------------------------------------*/

/* Scheduler includes. */

#ifdef __clang__
#pragma clang diagnostic ignored "-Wuninitialized"
#endif

#include "FreeRTOS.h"
#include "task.h"
#include "portmacro.h"
#include "encoding.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "peripheral.h"
#include <mt_printf.h>

#ifdef CFG_XGPT_SUPPORT
#include <xgpt.h>
#endif
#ifdef CFG_WDT_SUPPORT
#include <wdt.h>
#endif
#include <main.h>
#ifdef CFG_VCORE_DVFS_SUPPORT
#include <dvfs.h>
#endif

/* Contains context when starting scheduler, save all 31 registers */
#ifdef __gracefulExit
BaseType_t xStartContext[F_reg+ACC_reg+UAM_reg+HWDLP_reg_ctx+X_reg] = {0};
#endif

/*
 * The maximum number of tick periods that can be suppressed is limited by the
 * 24 bit resolution of the SysTick timer.
 */
#if configUSE_TICKLESS_IDLE == 1
	static uint32_t xMaximumPossibleSuppressedTicks = 0x7ffff;
#endif /* configUSE_TICKLESS_IDLE */

#ifdef configMultiHarts
int IRQ[portNUM_CPUS] = {0};
#else
int IRQ = 0;
#endif /* configMultiHarts */

#ifdef configExtension_NN
int VLENB = 0;
int NN_StackOffset = 0;
#endif /* configExtension_NN */

/*
 * Handler for timer interrupt
 */
void vPortSysTickHandler( void );

/*
 * Setup the timer to generate the tick interrupts.
 */
void vPortSetupTimer( void );

void vic_reset_priority(void);

/*
 * Used to catch tasks that attempt to return from their implementing function.
 */
void prvTaskExitError_pre( void ) __attribute__ ((used));
void prvTaskExitError( void ) __attribute__ ((used));

/*-----------------------------------------------------------*/
#if !defined(MRV)
/* Sets the next timer interrupt
 * Reads previous timer compare register, and adds tickrate */
static void prvSetNextTimerInterrupt(void)
{
    volatile uint64_t * mtime      = (uint64_t*) (CLINT_BASE_ADDR + CLINT_MTIME);
    volatile uint64_t * timecmp    = (uint64_t*) (CLINT_BASE_ADDR + CLINT_MTIMECMP);

    *timecmp = *mtime+(configTICK_CLOCK_HZ / configTICK_RATE_HZ);
}
#endif
/*-----------------------------------------------------------*/

/* Sets and enable the timer interrupt */
void vPortSetupTimer(void)
{
	int hartid = uxPortGetHartId();

	vic_enable();
#ifdef CFG_CPU_TICK_26M
        platform_set_hart_tick(hartid, 26000); /* @26M HZ */
#else
	platform_set_hart_tick(hartid, 33); /* @32K HZ */
#endif

	platform_hart_enable_tick(hartid);
	// Enable the Machine-External bit in MIE
	set_csr(mie, MIP_MEIP);
	// Enable the Machine-Timer bit in MIE
	set_csr(mie, MIP_MTIP);
	// Enable the Software interrupt bit in MIE
	set_csr(mie, MIP_MSIP);

	if(hartid == 0)
		platform_cpu_tick_enable();
}
/*-----------------------------------------------------------*/

/* setup a dummy operatation to re-order interrupts. */
void vic_reset_priority(void)
{
	vic_bit_clear_mask(0, 0x0);
}

#pragma GCC diagnostic push  // require GCC 4.6
#pragma GCC diagnostic ignored "-Wmissing-prototypes"
void prvTaskExitError_pre( void )
{
    prvTaskExitError();
}
#pragma GCC diagnostic pop   // require GCC 4.6

void prvTaskExitError( void )
{
	/* A function that implements a task must not exit or attempt to return to
	its caller as there is nothing to return to.  If a task wants to exit it
	should instead call vTaskDelete( NULL ).

	Artificially force an assert() to be triggered if configASSERT() is
	defined, then stop here so application writers can catch the error. */
	configASSERT( xGetCriticalNesting() == ~0UL );
	portDISABLE_INTERRUPTS();
	for( ;; );
}
/*-----------------------------------------------------------*/

/* Clear current interrupt mask and set given mask */
void vPortClearInterruptMask(int mask)
{
	__asm volatile("csrw mie, %0"::"r"(mask));
}
/*-----------------------------------------------------------*/

/* Set interrupt mask and return current interrupt enable register */
int vPortSetInterruptMask(void)
{
	int ret;
	__asm volatile("csrr %0,mie":"=r"(ret));
	__asm volatile("csrc mie,%0"::"r"(0xff030888));
	return ret;
}
/*-----------------------------------------------------------*/

#ifdef configExtension_NN
void xPortNNInitEnv(void)
{
	int NN_ELM;
	if(uxPortGetHartId() == 0) {
		VLENB = mrv_read_csr(0xC22);
		NN_ELM = VLENB/4; // vlenb = VLEN/8, num of element = VLEN/32 = vlenb*8/32 = vlenb/4
		NN_StackOffset = NN_reg * NN_ELM + NN_csr;

		__asm volatile("csrwi 0x01a, 0x0\n"
				"csrc mstatus, %0\n"
				::"r"(MSTATUS_VS_D):);
	}
}
#endif /* configExtension_NN */

/*
 * See header file for description.
 */
#ifdef configExtension_NN
StackType_t *pxPortInitialiseStack( StackType_t *pxTopOfStack, TaskFunction_t pxCode, void *pvParameters, int feature )
#else
StackType_t *pxPortInitialiseStack( StackType_t *pxTopOfStack, TaskFunction_t pxCode, void *pvParameters)
#endif /* configExtension_NN */
{
	/* Simulate the stack frame as it would be created by a context switch
	interrupt. */

	register int *tp asm("x3");
        //int size =

#ifdef configExtension_NN
	if(feature == feature_NN) {
		memset(pxTopOfStack-NN_StackOffset-F_reg_ctx-ACC_reg_ctx-UAM_reg_ctx-HWDLP_reg_ctx-X_reg_ctx, 0x0, (NN_StackOffset+F_reg_ctx+ACC_reg_ctx+UAM_reg_ctx+HWDLP_reg_ctx+X_reg_ctx) * 4);
		pxTopOfStack -= NN_StackOffset;
		*pxTopOfStack = 0x1;	/* unncken = 1*/
		pxTopOfStack-= (F_reg_ctx+ACC_reg_ctx+UAM_reg_ctx);
	} else {
		memset(pxTopOfStack-F_reg_ctx-ACC_reg_ctx-UAM_reg_ctx-HWDLP_reg_ctx-X_reg_ctx, 0x0, (F_reg_ctx+ACC_reg_ctx+UAM_reg_ctx+HWDLP_reg_ctx+X_reg_ctx) * 4);
		pxTopOfStack-= (F_reg_ctx+ACC_reg_ctx+UAM_reg_ctx);
	}
#else

	memset(pxTopOfStack-F_reg_ctx-ACC_reg_ctx-UAM_reg_ctx-HWDLP_reg_ctx-X_reg_ctx, 0x0, (F_reg_ctx+ACC_reg_ctx+UAM_reg_ctx+HWDLP_reg_ctx+X_reg_ctx) * 4);
	pxTopOfStack-= (F_reg_ctx+ACC_reg_ctx+UAM_reg_ctx);
#endif /* configExtension_NN */

	pxTopOfStack--;
	*pxTopOfStack = 3;					/* mlf = 3, unused */
	pxTopOfStack-=6;					/* mlc1, mle1, mls1, mlc0, mle0, mls0 */

#if defined(configExtension_NN)
	if(feature == feature_NN) {
		pxTopOfStack = ( StackType_t * ) ( ( ( portPOINTER_SIZE_TYPE ) pxTopOfStack ) & ( ~( ( portPOINTER_SIZE_TYPE ) (VLENB-1) ) ) );
	} else {
		pxTopOfStack = ( StackType_t * ) ( ( ( portPOINTER_SIZE_TYPE ) pxTopOfStack ) & ( ~( ( portPOINTER_SIZE_TYPE ) portBYTE_ALIGNMENT_MASK ) ) );
										}
#elif	defined(FF) || defined(FACC) || defined(FUAM) || defined(FHWDLP)
	pxTopOfStack = ( StackType_t * ) ( ( ( portPOINTER_SIZE_TYPE ) pxTopOfStack ) & ( ~( ( portPOINTER_SIZE_TYPE ) portBYTE_ALIGNMENT_MASK ) ) );
#endif

	pxTopOfStack--;						/* register mstatus */
#if defined(P_MODE_0) // task/RTOS in machine mode
	*pxTopOfStack = MSTATUS_MPP | MSTATUS_MPIE;
#elif defined(P_MODE_1) // task/RTOS in user mode
	*pxTopOfStack = MSTATUS_MPIE;
#endif /* P_MODE_0 */

#ifdef configExtension_F
	*pxTopOfStack |= MSTATUS_FS_C;
#endif /* configExtension_F */
#ifdef configExtension_ACC
	*pxTopOfStack |= MSTATUS_ACS_C;
#endif /* configExtension_ACC */
#ifdef configExtension_UAM
	*pxTopOfStack |= MSTATUS_UAS_C;
#endif /* configExtension_UAM */
#ifdef configExtension_NN
	if(feature == feature_NN) {
		*pxTopOfStack |= MSTATUS_VS_C;
	}
#endif /* configExtension_NN  */

	pxTopOfStack--;
	*pxTopOfStack = (portSTACK_TYPE)pxCode;			/* Start address */
	pxTopOfStack -= 22;
	*pxTopOfStack = (portSTACK_TYPE)pvParameters;		/* Register a0 */
	pxTopOfStack -= 6;
	*pxTopOfStack = (portSTACK_TYPE)tp; 			/* Register thread pointer */
	pxTopOfStack -= 3;
	*pxTopOfStack = (portSTACK_TYPE)prvTaskExitError;	/* Register ra */

	return pxTopOfStack;
}
/*-----------------------------------------------------------*/
//#include "dbg.h"
void vPortSysTickHandler( void )
{
#ifdef CFG_XGPT_SUPPORT
	unsigned long long ullTime_ns;
	TickType_t ulTimerTick, ulSystemTick, ulDelta;
#endif

#ifdef CFG_XGPT_SUPPORT
	ullTime_ns = read_xgpt_stamp_ns();
	ulTimerTick = (TickType_t)(ullTime_ns / 1000000);
	ulSystemTick = xTaskGetTickCount() + (TickType_t)GetPendedTicks();

	/* 0x10000000 is a randomly chosen value.
	 * Any value big enough to detect overflow can be used.
	 */
	ulDelta = ulTimerTick - ulSystemTick;
	if (ulDelta > 0x10000000) {
		/* Supress system tick count from incrementing. */
		//PRINTF_E("SysTick: %u, TimerTick: %u\n", ulSystemTick, ulTimerTick);
	} else
#endif
	{
#ifdef FREERTOS_SMP
		//mrv_printf("\n\r Hart%d: Tick! \n\r", uxPortGetHartId());
		/* Increment the RTOS tick. */
		BaseType_t xSwitchRequired;
		UBaseType_t uxSavedInterruptStatus;

		uxSavedInterruptStatus = portSET_INTERRUPT_MASK_FROM_ISR();
		xSwitchRequired = xTaskIncrementTick();

		if( xSwitchRequired != pdFALSE )
		{
			vTaskSwitchContext();
		}
		portCLEAR_INTERRUPT_MASK_FROM_ISR( uxSavedInterruptStatus );
#else
	/* Increment the RTOS tick. */
		if( xTaskIncrementTick() != pdFALSE )
		{
			vTaskSwitchContext();
		}
#endif
	}

	/* acknowledge the timer and recount for next tick */
#ifdef CFG_WDT_SUPPORT
#ifndef CFG_WDT_IDLE_KICK
	mtk_wdt_restart();
#endif
#endif
	platform_hart_tick_irq_ack(uxPortGetHartId());
}

#ifdef configMultiHarts

static volatile UBaseType_t 	cpu_swi_event[portNUM_CPUS] = { 0 };
static volatile UBaseType_t 	cpu_need_switch[portNUM_CPUS] = { 0 };
#if (configMultiHarts == 1)
static volatile UBaseType_t 	cpu_wakeup[portNUM_CPUS] = { 1 };
#else
static volatile UBaseType_t 	cpu_wakeup[portNUM_CPUS] = { 1, 0 };
#endif
static volatile TaskHandle_t 	cpu_add_tasks[portNUM_CPUS] = { 0 };

unsigned int cpu_swi_trigger = 0;
unsigned int cpu_swi_done = 0;

typedef struct
{
	volatile spinlock_t lock;
	int count;
	int cpuid;
} cpulock_t;
static cpulock_t critical_nesting = { {0}, 0, -1};

void vPortCPUSWIHandler( void )
{
	UBaseType_t uxSavedInterruptStatus;
	int cpuid = uxPortGetHartId();
	UBaseType_t swi_event;
	TaskHandle_t NewTask;

	if (IRQ[cpuid] > 1)
		configASSERT( 0 );

	/*************************************************************
	* CPU Context Switch
	*************************************************************/
	//mrv_printf("\n\r Hart%d: SWI! \n\r", cpuid);
	if (atomic_read(&cpu_need_switch[cpuid])) {
		atomic_set(&cpu_need_switch[cpuid], 0);
		uxSavedInterruptStatus = portSET_INTERRUPT_MASK_FROM_ISR();
		vTaskSwitchContext();
		portCLEAR_INTERRUPT_MASK_FROM_ISR( uxSavedInterruptStatus );
	}

	/*************************************************************
	* SWI handler
	*************************************************************/
	swi_event = atomic_read(&cpu_swi_event[cpuid]);
	if (swi_event != SWI_EVENT_NONE) {
		switch(swi_event) {
		case SWI_EVENT_WAKE_UP:
			atomic_set(&cpu_wakeup[cpuid], 1);
			break;
		case SWI_EVENT_ADDNEW_TCB:
			NewTask = atomic_read(&cpu_add_tasks[cpuid]);
			if (NewTask)
			{
				vAddNewTaskToCurrentReadyList(NewTask);
				atomic_set(&cpu_add_tasks[cpuid], NULL);
			}
			break;
		case SWI_EVENT_MASK_INT:
#if defined(CFG_VCORE_DVFS_SUPPORT) && !defined(CFG_HART1_DISABLE)
			/* PRINTF_D("SWI_EVENT_MASK_INT\n"); */
			if (cpuid == 1)
				hard1_intr_disable_flag = 1;
#endif
			break;
		case SWI_EVENT_UNMASK_INT:
#if defined(CFG_VCORE_DVFS_SUPPORT) && !defined(CFG_HART1_DISABLE)
			/* PRINTF_D("SWI_EVENT_UNMASK_INT\n"); */
			if (cpuid == 1)
				hard1_intr_disable_flag = 0;
#endif
			break;
		/*
		1. HART1 sw int to HART0
		2. HART0 go into ISR
		3. HART0 I/D $ barrier (ISR)
		4. HART0 OFF itself (ISR)
		5. HART1 check HRAT0 status (csr mharten)
		6. if HART0 off HART1 modify MPU
		7. HART1 turn on HART0
		*/
		case SWI_EVENT_SET_MPU:
			mbi();
			mrv_enable_cpus(0); /* OFF itself */
			break;

		default:
			break;
		}

		cpu_swi_done++;
		atomic_set(&cpu_swi_event[cpuid], SWI_EVENT_NONE);
		mb();
	}
}

void vPortCPURequestSwitch( int cpuid )
{
	atomic_set(&cpu_need_switch[cpuid], 1);
	/* trigger SWI for the CPU*/
	cpuid = 1 << (cpuid);
	//mrv_printf("\n\r Hart%d: ready to SWI:%d \n\r", uxPortGetHartId(), cpuid);
	/* TODO wait for compiler supports */
	//__asm volatile( "csrw miaswi, %0"::"r"(cpuid):);
	__asm volatile( "csrw 0x5c0, %0"::"r"(cpuid):);
	/* SW_INT needs 2T */
	__asm volatile( "nop");
	__asm volatile( "nop");
	__asm volatile( "nop");
}

void vPortCPUSleep( void )
{
	int cpuid = uxPortGetHartId();
	while (atomic_read(&cpu_wakeup[cpuid]) == 0)
	{
		asm volatile("wfi");
	}
}

void vPortSWIEvent( int cpuid, int event )
{
	while ((atomic_vcas((uint32_t *)&cpu_swi_event[cpuid], SWI_EVENT_NONE, event))
	       != SWI_EVENT_NONE) { ; }
	mb();
	/* trigger swi for the cpu */
	cpuid = 1 << (cpuid);
	/* TODO wait for compiler supports */
	//__asm volatile( "csrw miaswi_l, %0"::"r"(cpuid):);
	cpu_swi_trigger++;
	__asm volatile( "csrw 0x5c0, %0"::"r"(cpuid):);
	/* SW_INT needs 2T */
	__asm volatile( "nop");
	__asm volatile( "nop");
	__asm volatile( "nop");
}

void vPortAddNewTaskToCpuReadyList(int cpuid , void* pxTCB)
{
	while (atomic_read(&cpu_add_tasks[cpuid]));
	atomic_set(&cpu_add_tasks[cpuid], pxTCB);

	vPortSWIEvent(cpuid, SWI_EVENT_ADDNEW_TCB);
}

void vPortEnterCritical( void )
{
#if 0
	vTaskEnterCritical();
#else
	int cpuid = uxPortGetHartId();

cs_retry:
	if (!is_in_isr())
		vTaskEnterCritical();
	raw_spinlock_lock(&critical_nesting.lock);
	if (critical_nesting.count == 0)
	{
		/* First time get lock */
		critical_nesting.count++;
		critical_nesting.cpuid = cpuid;
	}
	else if (critical_nesting.cpuid == cpuid)
	{
		/* Same core get lock */
		critical_nesting.count++;
	}
	else {
		raw_spinlock_unlock(&critical_nesting.lock);
		if (!is_in_isr())
			vTaskExitCritical();
		while (atomic_read(&critical_nesting.count));
		goto cs_retry;
	}
	raw_spinlock_unlock(&critical_nesting.lock);
#endif
}

void vPortExitCritical( void )
{
#if 0
	vTaskExitCritical();
#else
	int cpuid = uxPortGetHartId();

	//Release the lock for critical
	raw_spinlock_lock(&critical_nesting.lock);
	if (critical_nesting.cpuid == cpuid)
	{
		/* Same core release lock */
		critical_nesting.count--;
		if (critical_nesting.count <= 0)
		{
			critical_nesting.cpuid = -1;
			critical_nesting.count = 0;
		}
	}
	raw_spinlock_unlock(&critical_nesting.lock);
	if (!is_in_isr())
		vTaskExitCritical();
#endif
	//portENABLE_INTERRUPTS();
}
/*-----------------------------------------------------------*/


int vPortSetInterruptMask_SMP(void)
{
	int ret;
	int cpuid = uxPortGetHartId();

	__asm volatile("csrr %0,mie":"=r"(ret));
	__asm volatile("csrc mie,%0"::"r"(0xff030888));

cs_retry:
	raw_spinlock_lock(&critical_nesting.lock);
	if (critical_nesting.count == 0)
	{
		/* First time get lock */
		critical_nesting.count++;
		critical_nesting.cpuid = cpuid;
	}
	else if (critical_nesting.cpuid == cpuid)
	{
		/* Same core get lock */
		critical_nesting.count++;
	}
	else {
		raw_spinlock_unlock(&critical_nesting.lock);
		while (atomic_read(&critical_nesting.count));
		goto cs_retry;
	}
	raw_spinlock_unlock(&critical_nesting.lock);

	return ret;
}

void vPortClearInterruptMask_SMP(int mask)
{
	int cpuid = uxPortGetHartId();

	//Release the lock for critical
	raw_spinlock_lock(&critical_nesting.lock);
	if (critical_nesting.cpuid == cpuid)
	{
		/* Same core release lock */
		critical_nesting.count--;
		if (critical_nesting.count <= 0)
		{
			critical_nesting.cpuid = -1;
			critical_nesting.count = 0;
		}
	}
	raw_spinlock_unlock(&critical_nesting.lock);

	__asm volatile("csrw mie, %0"::"r"(mask));
}
#endif /* configMultiHarts */
/*-----------------------------------------------------------*/

#if defined(PBFR_SUPPORT_IOSTALL) || defined(PBFR_SUPPORT_CACHE_COUNT)
/*
struct PMU_SC {
	bool cycle;
	bool instruction;
	bool hw_3;
	bool hw_4;
	bool hw_5;
	bool hw_6;
	bool hw_7;
};
struct PMU_CNT {
	unsigned int high;
	unsigned int low;
};
*/
struct PMU_SC SC;
#endif /* PBFR_SUPPORT_IOSTALL */

#ifdef PBFR_SUPPORT_IOSTALL
void vPortEnableIOStallRate(void);
void vPortDisableIOStallRate(void);
int vPortGetIOStallRate(void);

void vPortEnableIOStallRate(void)
{
    pmu_set_hw_event_mapping(3, 0);
    pmu_set_hw_event_mapping(4, 10);
    pmu_get_control(&SC);
    SC.cycle = 1;
    SC.hw_3 = 1;
    SC.hw_4 = 1;
    pmu_adjust_activaion(&SC);
}

void vPortDisableIOStallRate(void)
{
    pmu_get_control(&SC);
    SC.cycle = 0;
    SC.hw_3 = 0;
    SC.hw_4 = 0;
    pmu_adjust_activaion(&SC);
}

int vPortGetIOStallRate(void)
{
    struct PMU_CNT Total_Cycle_Count, LS_Cycle_Count_I, LS_Cycle_Count_D;
    unsigned long long Total, LS;
    int IO_loading;

    pmu_get_control(&SC);
    SC.cycle = 0;
    SC.hw_3 = 0;
    SC.hw_4 = 0;
    pmu_adjust_activaion(&SC);
    pmu_get_cycle_count(&Total_Cycle_Count);
    pmu_get_hw_count(3, &LS_Cycle_Count_I);
    pmu_get_hw_count(4, &LS_Cycle_Count_D);
    pmu_get_control(&SC);
    SC.cycle = 1;
    SC.hw_3 = 1;
    SC.hw_4 = 1;
    pmu_adjust_activaion(&SC);
    Total = (((unsigned long long)Total_Cycle_Count.high)<<32)+((unsigned long long)Total_Cycle_Count.low);
    LS = (((unsigned long long)LS_Cycle_Count_I.high)<<32)+((unsigned long long)LS_Cycle_Count_I.low);
    LS += (((unsigned long long)LS_Cycle_Count_D.high)<<32)+((unsigned long long)LS_Cycle_Count_D.low);
    IO_loading = (int)(LS*10000/Total);
    return IO_loading;
}
#elif defined(PBFR_SUPPORT_CACHE_COUNT)
void vPortEnableCacheCount(void);
void vPortDisableCacheCount(void);
void vPortHaltCacheCount(void);
void vPortResumeCacheCount(void);
unsigned long long vPortGetICacheAccessCount(void);
unsigned long long vPortGetICacheMissCount(void);
unsigned long long vPortGetDCacheAccessCount(void);
unsigned long long vPortGetDCacheMissCount(void);
int vPortGetCacheMissRate(void);

void vPortEnableCacheCount(void)
{
    pmu_set_hw_event_mapping(3, 1);
    pmu_set_hw_event_mapping(4, 2);
    pmu_set_hw_event_mapping(5, 3);
    pmu_set_hw_event_mapping(6, 11);
    pmu_set_hw_event_mapping(7, 12);
    pmu_get_control(&SC);
    SC.hw_3 = 1;
    SC.hw_4 = 1;
    SC.hw_5 = 1;
    SC.hw_6 = 1;
    SC.hw_7 = 1;
    pmu_adjust_activaion(&SC);
}

void vPortDisableCacheCount(void)
{
    pmu_get_control(&SC);
    SC.hw_3 = 0;
    SC.hw_4 = 0;
    SC.hw_5 = 0;
    SC.hw_6 = 0;
    SC.hw_7 = 0;
    pmu_adjust_activaion(&SC);
}

void vPortHaltCacheCount(void)
{
    pmu_get_control(&SC);
    SC.hw_3 = 0;
    SC.hw_4 = 0;
    SC.hw_5 = 0;
    SC.hw_6 = 0;
    SC.hw_7 = 0;
    pmu_adjust_activaion(&SC);
}

void vPortResumeCacheCount(void)
{
    pmu_get_control(&SC);
    SC.hw_3 = 1;
    SC.hw_4 = 1;
    SC.hw_5 = 1;
    SC.hw_6 = 1;
    SC.hw_7 = 1;
    pmu_adjust_activaion(&SC);
}

unsigned long long vPortGetICacheAccessCount(void)
{
    struct PMU_CNT I_Acc;
    unsigned long long ret;

    pmu_get_hw_count(3, &I_Acc);
    ret = (((unsigned long long)I_Acc.high)<<32)+((unsigned long long)I_Acc.low);
    return ret;
}

unsigned long long vPortGetICacheMissCount(void)
{
    struct PMU_CNT I_Miss_PFTH, I_Miss_EMI;
    unsigned long long ret;

    pmu_get_hw_count(4, &I_Miss_PFTH);
    pmu_get_hw_count(5, &I_Miss_EMI);
    ret = (((unsigned long long)I_Miss_PFTH.high)<<32)+((unsigned long long)I_Miss_PFTH.low);
    ret += (((unsigned long long)I_Miss_EMI.high)<<32)+((unsigned long long)I_Miss_EMI.low);
    return ret;
}

unsigned long long vPortGetDCacheAccessCount(void)
{
    struct PMU_CNT D_Acc;
    unsigned long long ret;

    pmu_get_hw_count(6, &D_Acc);
    ret = (((unsigned long long)D_Acc.high)<<32)+((unsigned long long)D_Acc.low);
    return ret;
}

unsigned long long vPortGetDCacheMissCount(void)
{
    struct PMU_CNT D_EMI;
    unsigned long long ret;

    pmu_get_hw_count(7, &D_EMI);
    ret = (((unsigned long long)D_EMI.high)<<32)+((unsigned long long)D_EMI.low);
    return ret;
}

int vPortGetCacheMissRate(void)
{
    struct PMU_CNT I_Acc, I_Miss_PFTH, I_Miss_EMI, D_Acc, D_EMI;
    unsigned long long Total, Miss;
    int Miss_rate;

    //PMU_disable();
    pmu_get_hw_count(3, &I_Acc);
    pmu_get_hw_count(4, &I_Miss_PFTH);
    pmu_get_hw_count(5, &I_Miss_EMI);
    pmu_get_hw_count(6, &D_Acc);
    pmu_get_hw_count(7, &D_EMI);
    //PMU_enable();
    Total = (((unsigned long long)I_Acc.high)<<32)+((unsigned long long)I_Acc.low);
    Total += (((unsigned long long)D_Acc.high)<<32)+((unsigned long long)D_Acc.low);
    Miss = (((unsigned long long)I_Miss_PFTH.high)<<32)+((unsigned long long)I_Miss_PFTH.low);
    Miss += (((unsigned long long)I_Miss_EMI.high)<<32)+((unsigned long long)I_Miss_EMI.low);
    Miss += (((unsigned long long)D_EMI.high)<<32)+((unsigned long long)D_EMI.low);
    Miss_rate = (int)(Miss*10000/Total);
    return Miss_rate;
}
#endif /* PBFR_SUPPORT_IOSTALL */


#if configUSE_TICKLESS_IDLE == 1
UBaseType_t uxIdleSavedIntStatus[portNUM_CPUS];

#define portMCU_WAKEUP_COST                ( 1 )
#define portTIME_SKEW_FACTOR               ( 10 )

__attribute__((weak)) void vPortSuppressTicksAndSleep( TickType_t xExpectedIdleTime )
{
	uint32_t ulCompleteTickPeriods;
	TickType_t xModifiableIdleTime;
	TickType_t ullAbsCurTick, ullAbsPreTick, ullDiffTick;
	UBaseType_t uxCpuId = uxPortGetHartId();

	/* Make sure the SysTick reload value does not overflow the counter. */
	if( xExpectedIdleTime > xMaximumPossibleSuppressedTicks )
	{
	    xExpectedIdleTime = xMaximumPossibleSuppressedTicks;
	}

    /* Stop the SysTick momentarily.  The time the SysTick is stopped for
     * is accounted for as best it can be, but using the tickless mode will
     * inevitably result in some tiny drift of the time maintained by the
     * kernel with respect to calendar time. */
	platform_hart_disable_tick(uxCpuId);

    /* Enter a critical section but don't use the taskENTER_CRITICAL()
     * method as that will mask interrupts that should exit sleep mode. */
    portDISABLE_INTERRUPTS();
#ifdef CFG_HART1_DISABLE
    vic_set_mask(0, 0x0);
#else
    if (uxCpuId == 0)
        vic_set_mask(0, 0x0);
    else
        vic_set_mask(0, 0x100); //unmask bit 8 for hart1 NMI
#endif

    /* If a context switch is pending or a task is waiting for the scheduler
     * to be unsuspended then abandon the low power entry. */
    if( eTaskConfirmSleepModeStatus() == eAbortSleep )
    {
        /* Restart SysTick. */
        platform_hart_enable_tick(uxCpuId);
    } else {
        /* If xExpectedIdleTime less than MCU-wake-up time, it had better
         * loop rather than go to sleep.
         */
        if (xExpectedIdleTime > portMCU_WAKEUP_COST)
        {
            xModifiableIdleTime = xExpectedIdleTime;
            /* Sleep until something happens.  configPRE_SLEEP_PROCESSING() can
             * set its parameter to 0 to indicate that its implementation contains
             * its own wait for interrupt or wait for event instruction, and so wfi
             * should not be executed again.  However, the original expected idle
             * time variable must remain unmodified, so a copy is taken. */
#ifdef CFG_WDT_SUPPORT
            mtk_wdt_disable();
#endif
#ifdef CFG_PLAT_TIMER_V2
            //platform_set_periodic_timer(sleep_wakeup_timer_fun, (void *) 0, xModifiableIdleTime);
            plat_timer_set(TICK_TIMER + uxPortGetHartId(), xModifiableIdleTime);
#endif

#if defined(CFG_VCORE_DVFS_SUPPORT) || defined(CFG_SLEEP_CALLBACK_SUPPORT)
            if (configPRE_SLEEP_PROCESSING( xModifiableIdleTime ) == 1) {
#endif

                mrv_dcache_barrier();
                __asm volatile( "wfi" );
                mrv_icache_barrier();
#if defined(CFG_VCORE_DVFS_SUPPORT) || defined(CFG_SLEEP_CALLBACK_SUPPORT)
            }
            configPOST_SLEEP_PROCESSING( xModifiableIdleTime );
#endif

#ifdef CFG_WDT_SUPPORT
            mtk_wdt_restart();
            mtk_wdt_enable();
#endif

            ullAbsPreTick = xTaskGetTickCountFromISR();
            /* absolute ticks (ms) from start */
            ullAbsCurTick = read_xgpt_stamp_ns() / 1000000;

            /* Time Guard:
             * Somehow xgpt gives reverse time which makes former given
             * time, xTickCount, begger than the latter given time. In this
             * case sync current time to previous time because it'd better
             * not modify the original tick count.
             */
            if (ullAbsPreTick > ullAbsCurTick) {
                /* if time reverse happens, the difference of previous tick and
                 * current tick would be very small, or otherwise current tick
                 * would be the value for overflow.
                 */
                if (ullAbsPreTick - ullAbsCurTick < portTIME_SKEW_FACTOR)
            	ullAbsCurTick = ullAbsPreTick;
            }

            ullDiffTick = ullAbsCurTick - ullAbsPreTick;

            /* The unblock time could be updated by task migration,
             * so update the modified time again.
             */
            xModifiableIdleTime = xTaskGetExpectedIdleTime();

            /* mtime < difftime: sleep overhead makes current time exeeding the
             *		     expected time.
             * mtime > difftime: being disturbed by some interrupt events.
             * mtime = difftime: wake up on time, so minus one for ready tasks
             *		     to be selected by xTaskIncrementTick.
             */
            ulCompleteTickPeriods =
                (xModifiableIdleTime < ullDiffTick) ? xModifiableIdleTime :
                (xModifiableIdleTime > ullDiffTick) ? ullDiffTick :
            					 ullDiffTick - 1;

            /* To step to xNextTaskUnblockTime in one go */
            vTaskStepTick( ulCompleteTickPeriods );

            /* The remain tick count says how many ticks exceed unblock time and
             * there must be some tasks who are needed to be waken up.
             */
            while (ulCompleteTickPeriods < ullDiffTick) {
                ulCompleteTickPeriods++;
                /* The increment of ticks is to only increase the variable,
                 * uxPendedTicks. The value will be taken in the following
                 * function, xTaskResumeAll, and then put ready tasks in ready
                 * queue.
                 */
                xTaskIncrementTick();
            }

            /* Clear interrupt and restart SysTick. */
            platform_hart_tick_irq_ack(uxCpuId);
        }

	    /* Restart SysTick for sleep or busy loop cases in the else scope. */
	    platform_hart_enable_tick(uxCpuId);
    }

	/* Re-enable interrupts - see comments above the cpsid instruction()
	 * above.
	 */
#ifdef CFG_HART1_DISABLE
	vic_set_mask(0, 0x3fff);
#else
	if (uxCpuId == 0)
		vic_set_mask(0, 0xff);
	else
		vic_set_mask(0, 0x3f00);
#endif
	portENABLE_INTERRUPTS();
}

#endif /* #if configUSE_TICKLESS_IDLE */
